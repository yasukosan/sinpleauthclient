import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { LocalStrageService } from '../_lib_service';

@Injectable()
export class RestService {

    private CSRF_TOKEN = '';
    private httpOptions: any = {};
    // private server: string = 'http://localhost:8001';
    private server: string = 'http://localhost:8000';
    private path: string = '';

    private responseStatus: any[] = [];
    private responseData: any;
    private responseCode = 200;

    /**
     * コンストラクタ. HttpClientService のインスタンスを生成する
     *
     * @param {Http} http Httpサービスを DI する
     * @memberof HttpClientService
     */
    constructor(
        private http: HttpClient,
        private localstrageService: LocalStrageService
    ) {
        this.makeHeader();
        this.setAuthorization('my-auth-token');
    }

    private makeHeader(): void {
        this.httpOptions = {
            headers: new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': this.CSRF_TOKEN
            }),
            body: null
        };
    }

    public setpath(path: string): void {
        this.path = path;
    }

    public setCSRF(token): void {
        this.CSRF_TOKEN = token;
    }

    /**
     * HTTP GET メソッドを実行する
     * (toPromise.then((res) =>{}) を利用する場合のコード)
     *
     * @returns {Promise<any[]>}
     * @memberof HttpClientService
     */
    public get(id: number = null): Promise<any> {
        let url = this.server + this.path;
        console.log(url);
        if (id !== null) {
            url = url + '/' + id;
        }
        return this.http
            .get(
                url,
                this.httpOptions
            )
            .toPromise()
            .then((response) => {
                return response;
            })
            .catch((response) => {
                this.errorHandler(response);
                return false;
            });
    }

    public post(data: any): Promise<any> {
        const url = this.server + this.path;
        return this.http
            .post(
                url,
                JSON.stringify(data),
                this.httpOptions )
            .toPromise()
            .then(response => {
                return response;
            })
            .catch(response => {
                return response;
            });
    }
    /**
     * PUTメソッド
     *
     * @returns {Promise<any[]>}
     */
    public put(data: any): Promise<any> {

        return this.http
            .put(this.server + this.path + '/' + data['id'],
                JSON.stringify(data),
                this.httpOptions )
            .toPromise()
            .then(response => {
                return response;
            })
            .catch(response => {
                return response;
            });
    }
    /**
     * DELETEメソッド実行
     *
     * @returns {Promise<any[]>}
     */
    public delete(id): Promise<any> {
        return this.http
            .delete(this.server + this.path + '/' + id,
                this.httpOptions )
            .toPromise()
            .then(response => {
                return response;
            })
            .catch(response => {
                return response;
            });
    }


    public getCSRF(): void {
        this.http
            .get(this.server + '/ct')
            .toPromise()
            .then(response => {
                console.log(response);
                this.CSRF_TOKEN = response['token'];
            })
            .catch(response => {
                console.log(response);
            });
    }



    /** */
    public oauth(): Promise<any> {
        return this.http
            .get(this.server + '/oauth/clients')
            .toPromise()
            .then(response => {
                console.log(response);
            });
    }

    public getToken(data: string = null): Promise<any> {
        let httpOptions = {};
        if (data !== null) {
            httpOptions = {
                headers: new HttpHeaders(
                {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + data
                }),
                body: null
            };
        } else {
            httpOptions = {
                headers: new HttpHeaders(
                {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiI' +
                    'sImp0aSI6IjE2MTFkMjY4MWY3YTkxOGY1NjkyZWM4MTM2ZmJhNTMzMjczND' +
                    'E4NWE4MjJhZGYxYTRiNzM0MTYzYTQzMjZkMGIzN2YwZTg3MmNhMjlmMjcxI' +
                    'n0.eyJhdWQiOiIxMSIsImp0aSI6IjE2MTFkMjY4MWY3YTkxOGY1NjkyZWM4' +
                    'MTM2ZmJhNTMzMjczNDE4NWE4MjJhZGYxYTRiNzM0MTYzYTQzMjZkMGIzN2Y' +
                    'wZTg3MmNhMjlmMjcxIiwiaWF0IjoxNTU5ODA3MjY5LCJuYmYiOjE1NTk4MD' +
                    'cyNjksImV4cCI6MTU2MTEwMzI2OSwic3ViIjoiNCIsInNjb3BlcyI6W119.' +
                    'OJcSBXjXAtiZSf6SpG3j8y3T9eMHfPfhWpNGY5z9niqYUK9SfbCnr6jE1Sh' +
                    'VcvGO_OC-OC7-UrY59wFqdWTNHLooGj0of8UKIQls7R_bgjsNSYQQPsQFhp' +
                    'N2CGoKBoFiB81CCUtovbHz7XWXzPzTRkCH_dYJlQTBMKXc44juvcH7_pois' +
                    'tVqy7MdOJDW3KXpbjJVOONAo0igP7kkgpGJpW3echtF5MCo_NjamRETAgNn' +
                    'GiHhHlORp2YvSNi29KAYeSt8QgjL3uU2aTiUc0If7nOrvlT-cscQD58wnBU' +
                    'ts50peIVuQdb4WdwwUJr9THMpww90W6BVyIlAwc5YBfTVhKCSZ5PzAJlEMG' +
                    'Ssh1_eIu5Ko7-HU0Hau3kVZ4N9KVShJnHW-cG8feip1Rz6V1Z9bNduUqyZ0' +
                    'qjJOkymvE75bXEnXD14emVOkQLrMVWoKFAxVrB92mxOXDKSCwLgDkN1yEY2' +
                    'C5qKg_IgSEHhJcnC1U7zwS38bn3FtoLl-85FSIShT3p6Ea_TEXg5P27dMIp' +
                    'OtLSwJooCanr-hC7oLyH1mIA_xs0ol6PzIEf3BqUUYIDWQM2zm5tii6pHwn' +
                    'cx_WJvpx4dqBmqr2gAlkvKfe5IA2qJgqNV_UqyRh16MHlXPPUqtmImkYam1' +
                    'a0Fp1FbrnEDRgP70zwTfKlSpIGJPnxztnU'
                }),
                body: null
            };
        }

        return this.http
            .get(
                this.server + '/api/user',
                // this.server + '/',
                httpOptions
            )
            .toPromise()
            .then(response => {
                console.log(response);
            })
            .catch(response => {
                console.log(response);
            });
    }

    /**
     * 
     */
    public login(username: string, password: string): Promise<any> {
        this.makeHeader();

        const form = {
            username    : username,
            password    : password,
            _token      : this.CSRF_TOKEN,
        };
        return this.http
            .put(
                this.server + '/api/login',
                JSON.stringify(form),
                this.httpOptions)
            .toPromise()
            .then(response => {
                if ('access_token' in response) {
                    this.getToken(response['access_token']);
                    return response['access_token'];
                }
                return false;
            })
            .catch(response => {
                console.log(response);
            });
    }
    public test(): Promise<any> {
        return this.http
            .get(
                this.server + '/users/test',
                this.httpOptions)
            .toPromise()
            .then(response => {
                console.log(response);
            })
            .catch(response => {
                console.log(response);
            });
    }
    public logout(): Promise<any> {
        return this.http
            .post(this.server + '/api/users/logout', '')
            .toPromise()
            .then(response => {
                console.log(response);
            })
            .catch(response => {
                console.log(response);
            });
    }

    public testGetClients(): void {
        this.http
            .get(this.server + '/oauth/clients')
            .toPromise()
            .then(response => {
                console.log(response);
            })
            .catch(response => {
                console.log(response);
            });
    }
    public testAddClients(): void {
        const data = {
            name: 'TestClient1',
            redirect: 'http://example/com'
        };
        this.http
            .post(this.server + '/oauth/clients',
            JSON.stringify(data),
            this.httpOptions)
            .toPromise()
            .then(response => {
                console.log(response);
            })
            .catch(response => {
                console.log(response);
            });
    }

    public testUpdateClients(id): void {
        const data = {
            name: 'TestClient1',
            redirect: 'http://example/com'
        };
        this.http
            .put(this.server + '/oauth/clients/' + id,
            JSON.stringify(data),
            this.httpOptions)
            .toPromise()
            .then(response => {
                console.log(response);
            })
            .catch(response => {
                console.log(response);
            });
    }

    public testDeleteClients(id): void {
        this.http
            .delete(this.server + '/oauth/clients/' + id)
            .toPromise()
            .then(response => {
                console.log(response);
            })
            .catch(response => {
                console.log(response);
            });
    }

    public testRequestToken(): void {

    }

    /**
     * REST-API 実行時のエラーハンドラ
     * (toPromise.then((res) =>{}) を利用する場合のコード)
     *
     * @private
     * @param {any} err エラー情報
     * @memberof HttpClientService
     */
    private errorHandler(err) {
        console.log('Error occured.', err);
        // return Promise.reject(err.message || err);
    }

    /**
     * Authorizatino に認証トークンを設定しする
     *
     * @param {string} token 認証トークン
     * @returns {void}
     * @memberof HttpClientService
     * @description
     * トークンを動的に設定できるようメソッド化している
     * Bearer トークンをヘッダに設定したい場合はこのメソッドを利用する
     */
    public setAuthorization(token: string = null): void {
        if (!token) {
            return;
        }
        const bearerToken: string = `Bearer ${token}`;
            this.httpOptions.headers = this.httpOptions.headers.set('Authorization', bearerToken);
    }
}
