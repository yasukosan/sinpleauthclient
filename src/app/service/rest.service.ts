import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
// import { Rest } from './rest';

@Injectable()

export class RestService {

    private debug = true;
    private serverUrl = 'server/aj';
    private sendData = '';
    private headers = '';

    private RequestData: any;
    private HttpHeader: HttpHeaders;

    private responseStatus: any[] = [];
    private responseData: any;

    constructor(private http: HttpClient) {}

    public setServerURL(server: string): void
    {
        this.serverUrl = server;
    }

    public setHeaders(header: string): void
    {
        this.headers = header;
    }

    public setParam(data: string): void
    {
        this.sendData = data;
    }

    /**
     * @returns {Promise<any[]>}
     */
    public getRequest(): Promise<any> {
        this.buildHeader();
        this.buildSendDataToJson();

        const data = this.jsonToURLParam(this.RequestData);

        return this.http
            .get(this.serverUrl + data,
                { 
                    headers: this.HttpHeader,
                    observe: 'response'
                })
            .toPromise()
            .then(response => {
                return this.resultParser(response, 'success');
            })
            .catch(response => {
                console.log(response.headers);
                return this.resultParser(response, 'error');
            });
    }

    /**
     * Postリクエスト処理
     * 
     * @return Promise<any>
     */
    public postRequest(): Promise<any> {
        this.buildHeader();
        this.buildSendDataToJson();

        return this.http
            .post(this.serverUrl,
                this.RequestData,
                { 
                    headers: this.HttpHeader,
                    observe: 'response'
                } )
            .toPromise()
            .then(response => {
                return this.resultParser(response, 'success');
            })
            .catch(response => {
                return this.resultParser(response, 'error');
            });
    }

    /**
     * Put リクエストの送信
     * 
     * @return Promise<any>
     */    
    public putRequest(): Promise<any> {
        this.buildHeader();
        this.buildSendDataToJson();
        
        return this.http
            .put(this.serverUrl,
                this.RequestData,
                { 
                    headers: this.HttpHeader,
                    observe: 'response'
                } )
            .toPromise()
            .then(response => {
                return this.resultParser(response, 'success');
            })
            .catch(response => {
                return this.resultParser(response, 'error');
            });
    }
    /**
     * Deleteリクエストの送信
     * 
     * @return Promise<any>
     */
    public deleteRequest(): Promise<any> {
        this.buildHeader();
        this.buildSendDataToJson();
        
        const data = this.jsonToURLParam(this.RequestData);

        return this.http
            .delete(this.serverUrl + data,
                { 
                    headers: this.HttpHeader,
                    observe: 'response'
                } )
            .toPromise()
            .then(response => {
                return this.resultParser(response, 'success');
            })
            .catch(response => {
                return this.resultParser(response, 'error');
            });
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }

    /**
     * サーバーからの戻りデータを配列に変換し返す
     * 
     * @param result 
     * @param mode 
     * @return Object{response:any , status:Int , statusText:string}
     */
    private resultParser(result: any, mode: string = 'success'): object {
        console.log(result);
        const data = {response: '', status: 0, statusText: ''};
        if (mode === 'success') {
            data.response = result.body;
        } else if (mode === 'error') {
            console.log(result);
            data.response = result.error;
        }
        data.status = result.status;
        data.statusText = result.statusText;
        return data;
    }

    private pullConsole(mess: any): void {
        if (this.debug) {
            console.log(mess);
        }
    }

    private jsonToURLParam(json: string): string
    {
        if (json.length < 5) {
            return '';
        }
        const _json = JSON.parse(json);
        let param = '?';
        for (const key in _json) {
            if (_json.hasOwnProperty(key)) {
                param = param + key + '=' + _json[key] + '&';
            }
        }
        return param;
    }

    private parseSendData(): any
    {
        let sendData = {};
        const _sendData = this.sendData.split(/\n/);

        for (const key in _sendData) {
            if (_sendData.hasOwnProperty(key)) {
                const val = _sendData[key].split('=');
                sendData[val[0]] = val[1];
            }
        }
        console.log(sendData);
        return sendData;
    }

    private buildSendDataToJson(): void
    {
        this.RequestData = JSON.stringify(this.parseSendData());
    }

    /**
     * フォームのヘッダー情報を配列に変換する
     */
    private parseHeader(): any
    {
        if (this.headers == '') {
            return {};
        }
        let headerObj = {};
        const _header = this.headers.split(/\n/);

        for (const key in _header) {
            if (_header.hasOwnProperty(key)) {
                const val = _header[key].split(':');
                headerObj[val[0]] = val[1];
            }
        }
        return headerObj;
    }

    /**
     * ヘッダー配列からHttpHeadrsオブジェクトを作成
     */
    private buildHeader(): void
    {
        this.HttpHeader = new HttpHeaders(this.parseHeader());
    }

}
