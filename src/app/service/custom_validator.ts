import { Injectable } from '@angular/core';

@Injectable()
export class CustomValidator {

    private Form;
    private InvalidForm = false;
    private validate;
    private message = [];
    private name;

    private errorMessages = {
        required: '必須項目です',
        maxLength: '文字が多すぎます',
        minLength: '文字数不足',
        match: '入力内容が一致しません',
        email: 'メール形式ではありません',
        haveBlank: '空白を含まれていません',
    };

    set FormModel(form) {
        this.Form = form;
    }
    set FormValidator(validator) {
        this.validate = validator;
    }

    public ErrorMessage(name): object {
        return this.message[name];
    }

    get FormError(): boolean {
        return this.InvalidForm;
    }

    public check(name): boolean {
        this.name = name;
        this.InvalidForm = false;
        for (const key in this.validate) {
            if (this.validate.hasOwnProperty(key)) {
                if (String(name) === String(key)) {
                    this.message[name] = [];
                    this.doValidate(this.validate[key], name);
                }
            }
        }
        if (this.message[name] && this.message[name].length > 0) {
            return true;
        }
        return false;
    }
    private doValidate(validate: object, name: string): void {
        for (const key in validate) {
            if (validate.hasOwnProperty(key)) {
                if (Array.isArray(validate[key])) {
                    this[ validate[key].slice(0, 1) ]( validate[key].slice(1));
                } else {
                    this[validate[key]]();
                }
            }
        }
    }
    private pushMessage(name): void {
        this.falseInvalid();
        this.message[this.name].push(this.errorMessages[name]);
    }
    private falseInvalid(): void {
        this.InvalidForm = true;
    }
    private required(): boolean {
        if (this.Form[this.name] !== '') {
            return false;
        } else {
            this.pushMessage('required');
            return true;
        }
    }

    private maxLength(obj): boolean {
        if (obj > 0) {
            if (this.Form[this.name].length <= obj) {
                return false;
            } else {
                this.pushMessage('maxLength');
                return true;
            }
        }
    }
    /**
     * 文字数が最低数以上あるか確認
     * @param obj String
     * @return boolean
     */
    private minLength(obj): boolean {
        if (obj >= 0) {
            if (this.Form[this.name].length >= obj) {
                return false;
            } else {
                this.pushMessage('minLength');
                return true;
            }
        }
    }
    /**
     * メール形式になっているか確認
     * @param obj String
     * @return boolean
     */
    private email(obj): boolean {
        if (obj !== '') {
            if (this.Form[this.name].match(/.+@.+\..+/)) {
                return false;
            } else {
                this.pushMessage('email');
                return true;
            }
        }
    }
    /**
     * 文字列の間にスペースが含まれるか確認
     * @param obj String
     * @return boolean
     */
    private haveBlank(obj): boolean {
        if (obj !== '') {
            const value = obj;
            // 両端のスペースを取り除く
            const name = value.trim();
            // 半角スペース
            const NAME_COLUMN_SPRIT_VALUE = ' ';
            // 全角スペース
            const NAME_COLUMN_SPRIT_VALUE_W = '　';
            let isError = false;
            if (name.indexOf(NAME_COLUMN_SPRIT_VALUE) < 0
            && name.indexOf(NAME_COLUMN_SPRIT_VALUE_W) < 0) {
                // 半角スペースも全角スペースも含まれていない場合
                isError = true;
            }
            // 姓と名の間にスペースが入力されていない場合は、バリデーションエラーを返す
            if (isError) {
                this.pushMessage('hasBlank');
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * パスワードと確認用パスワードが一致するかチェック
     * フォーム間の一致チェックに応用可
     * @param obj Object
     * @return boolean
     */
    private match(obj) {
        if (this.Form[obj[0]] !== this.Form[obj[1]]) {
            this.pushMessage('match');
            return true;
        } else {
            return false;
        }
    }

    private unknownError() {

    }
}
