import { Injectable } from '@angular/core';

@Injectable()
export class LocalStrageService {

    private strageData: any = [];

    /**
     *
     */
    constructor() {}

    /**
     * 
     * 単一データの操作
     * 
     */

    /**
     * データ保存
     * @param key string キー
     * @param data string データ
     */
    public save(key, data): void {
        localStorage.setItem(key, data);
    }

    /**
     * JSONデータを保存
     * @param key string キー
     * @param obj string JSON
     */
    public saveToJson(key, obj): void {
        this.save(key, this.toJson(obj));
    }

    /**
     * データ取得（文字列データ）
     * @param key string キー
     */
    public load(key): string {
        return localStorage.getItem(key);
    }

    /**
     * データ取得（JSONデータ）
     * @param key string キー
     */
    public loadToJson(key): object {
        return this.fromJson(this.load(key));
    }

    /**
     * データ削除
     * @param key string キー
     */
    public delete(key): void {
        localStorage.removeItem(key);
    }


    /**
     * 
     * 疑似簡易DB用
     * 
     */

    /**
     * 指定したkeyにレコードを追加
     * 
     * @param key 
     * @param data 
     */
    public addData(key: string, data: JSON): object
    {
        this.loadData(key);
        this.strageData.push(data);
        localStorage.setItem(key, this.toJson(this.strageData));
        return this.loadData(key);
    }

    /**
     * keyに一致するレコードを返す
     * 
     * @param key 
     */
    public loadData(key: string): object
    {
        this.strageData = this.fromJson(localStorage.getItem(key));
        if (this.strageData === null) {
            this.strageData = [];
        }
        return this.strageData;
    }

    /**
     * レコードを削除
     * 
     * @param key 
     * @param id 
     */
    public deleteData(key: string, id: number): object
    {
        this.loadData(key);
        this.strageData.splice(id, 1);
        localStorage.setItem(key, this.toJson(this.strageData));
        return this.loadData(key);
    }

    /**
     * 登録レコード内を検索し配列を返す
     * 
     * @param key localstrageのkey
     * @param calam 検索対象のカラム
     * @param word 検索ワード(正規表現不可)
     */
    public searchData(key: string, calam: string, word: string): object
    {
        this.loadData(key);
        const result = [];

        for (const key in this.strageData) {
            if (this.strageData.hasOwnProperty(key)) {
                if (calam in this.strageData[key]) {
                    if (this.strageData[key][calam].indexOf(word) != -1) {
                        result.push(this.strageData[key]);
                    }
                }
            }
        }

        return result;
    }

    

    /**
     * JSONデータを文字列に変換
     * @param obj JSON
     * @return string
     */
    private toJson(obj: Object): string {
        return JSON.stringify(obj);
    }

    /**
     * 文字列をJSONに変換
     * @param txt string 文字列
     * @return JSON
     */
    private fromJson(txt: string): object {
        return JSON.parse(txt);
    }
}
