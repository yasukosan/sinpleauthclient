import { Component } from '@angular/core';

import { RestService } from './service/rest.service';
import { LocalStrageService } from './_lib_service/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public resultStatus = '';
  public resultStatusText = '';
  public resultHeader = [];
  public resultData = [];

  public authSetting: any = [];
 
  public requests = {
    // server: 'https://saijyou.192.168.2.93.nip.io',
    server: 'https://localhost/sinpleAuthClient/server/public/api',
    header: 'Content-Type: application/json',
    json: 'anpan=man'
  };
  

  public constructor(
    private restService: RestService,
    private localStrage: LocalStrageService
  ) {
    this.loadSetting();
  }

  /**
   * GetRequestの送信
   */
  public sendGetRequest(): void {
    this.setRequestState();
    this.restService.getRequest()
    .then(result => {
      console.log(result);
      this.setResult(result);
    });
  }
  /**
   * PostRequestの送信
   */
  public sendPostRequest(): void {
    this.setRequestState();
    this.restService.postRequest()
    .then(result => {
      console.log(result);
      this.setResult(result);
    });
  }
  /**
   * PutRequestの送信
   */
  public sendPutRequest(): void {
    this.setRequestState();
    this.restService.putRequest()
    .then(result => {
      console.log(result);
      this.setResult(result);
    });
  }
  /**
   * DeleteRequestの送信
   */
  public sendDeleteRequest(): void {
    this.setRequestState();
    this.restService.deleteRequest()
    .then(result => {
      console.log(result);
      this.setResult(result);
    });
  }



  /**
   * 設定をローカルストレージに保存
   */
  public saveSetting(): void
  {
    this.authSetting.push(this.requests);
    this.localStrage.saveToJson('simple', this.authSetting);
    this.loadSetting();
  }

  /**
   * 設定を反映する
   */
  public callSetting(id: number): void
  {
    this.requests.server = this.authSetting[id].server;
    this.requests.header = this.authSetting[id].header;
    this.requests.json = this.authSetting[id].json;
  }

  /**
   * 設定をローカルストレージから呼出
   */
  public loadSetting(): void
  {
    this.authSetting = this.localStrage.loadToJson('simple');
    if (this.authSetting === null) {
      this.authSetting = [];
    }
  }

  public deleteSetting(id: number): void
  {
    this.authSetting.splice(id, 1);
    this.localStrage.saveToJson('simple', this.authSetting);
    this.loadSetting();
  }



  /**
   * サーバーからの戻りを出力
   * @param result 
   */
  private setResult(result): void
  {
    // ステータスコード出力
    this.resultStatus = result.status;
    // ステータス文字列出力
    this.resultStatusText = result.statusText;
    // レスポンスヘッダー出力
    this.resultHeader = this.objectToArray(result.response.header);
    // レスポンスボディー出力
    this.resultData = this.objectToArray(result.response.data);
  }

  /**
   * 配列を改行コードを含めた文字列に変換し返す
   * 
   * @param data 
   * @return string
   */
  private arrayToString(data: object): string
  {
    let txt = '';
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        txt = txt + key + ': ' + data[key] + '  \n';
      }
    }
    return txt;
  }

  /**
   * objectデータを配列<文字列>に変換し返す
   * 
   * @param data 
   * @return Array<string>
   */
  private objectToArray(data: object): Array<string>
  {
    const _data = [];
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        _data.push(key + ': ' + data[key]);
      }
    }
    return _data
  }

  /**
   * サーバーリクエストをrest.serviceに登録
   */
  private setRequestState(): void
  {
    this.restService.setServerURL(this.requests.server);
    this.restService.setHeaders(this.requests.header);
    this.restService.setParam(this.requests.json);
  }
}
