import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RestService, UserService, CustomValidator } from './service';
import { LocalStrageService } from './_lib_service';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    RestService, UserService, CustomValidator,
    LocalStrageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
