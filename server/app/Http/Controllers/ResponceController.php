<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResponceController extends Controller
{

    /**
     * getリクエスト処理
     *
     * @param Request $request
     * @return array
     */
    public function getRequest(Request $request): array
    {
        return $this->buildReturnData($request);
    }

    /**
     * postリクエスト処理
     *
     * @param Request $request
     * @return array
     */
    public function postRequest(Request $request): array
    {
        return $this->buildReturnData($request);
    }

    /**
     * putリクエスト処理
     *
     * @param Request $request
     * @return array
     */
    public function putRequest(Request $request): array
    {
        return $this->buildReturnData($request);
    }

    /**
     * deleteリクエスト処理
     *
     * @param Request $request
     * @return array
     */
    public function deleteRequest(Request $request): array
    {
        return $this->buildReturnData($request);
    }

    /**
     * リターンレスポンスの作成
     *
     * @param [type] $request
     * @return array
     */
    private function buildReturnData($request): array
    {
        return array(
            'data'      => $request->all(),
            'header'    => getallheaders()
        );
    }
}
